# Assignment #10
# William Krug
# CSCI 2001-90
# compute the GCD (Greatest Common Divisor) for two numbers

def main():
   number1 = eval(input("Enter the first number. Type -999 to quit: "))
   
   while number1 != -999:
      number2 = eval(input("Enter the second number: "))
      
      greatestCommonDivisor = gcd(number1, number2)

      if greatestCommonDivisor != None:
         print("\nThe GCD (Greatest Common Divisor) of", number1, "and",
               number2, "is", greatestCommonDivisor)
      else:
         print("The GCD (Greatest Common Divisor) of", number1, "and",
               number2, "is", None)

      print()
      number1 = eval(input("Enter the first number. Type -999 to quit: "))
      

# This function finds the GCD of two numbers
def gcd(m, n):
   # Check for division by zero
   if n == 0:
      print("\nError: Division by zero is not possible")
      return None

   # Check if n is the GCD
   elif m % n == 0:
      return n

   # Compute GCD thru recursion
   else:
      return gcd(n, m % n)

main()