# Assignment #4
# William Krug
# CSCI 2001-90
# Calculate the roots of a quadratic equation

import math

# Prompt user if they want to find a root
findRoot = input("Do you wish to find a quadratic root? Enter 'Y' to continue: ")

while findRoot.upper() == 'Y':
   # Prompt user for the 'a', 'b', and 'c' coefficients
   print()
   a = eval(input("Enter the \'a\' coefficient of your quadratic equation " +
                  "(ax^2+bx+c): "))
   b = eval(input("Enter the \'b\' coefficient of your quadratic equation " +
                  "(ax^2+bx+c): "))
   c = eval(input("Enter the \'c\' coefficient of your quadratic equation " +
                  "(ax^2+bx+c): "))

   # Check if the equation is quadratic or linear
   if a == 0:
      print("\nYou entered a linear equation...")
      linearAnswer = input("Do you want the root of the linear equation? " +
                           "Enter 'Y' to continue: ")

      # Calculate linear root
      if linearAnswer.upper() == 'Y':
         if b == 0:
            print("\nThere is no root, this is a constant value: y =", c)
         else:
            linearRoot = (-c) / b

            #Display linear root
            print("\nThe linear root is:", format(linearRoot, ".2f"))

      # Terminate iteration
      else:
         print("\nBetter luck next time")

   else:
      # Determine the value of the discriminant
      discriminant = (b * b) - (4 * a * c)

      #Calculate quadratic roots
      if discriminant < 0:
         print("\nYour equation has imaginary roots...")
         imaginaryAnswer = input("Do you want to find the imaginary roots? " +
                                 "Enter 'Y' to continue: ")

         # Calculate imaginary roots
         if imaginaryAnswer.upper() == 'Y':
            imaginaryRootReal = (-b) / (2 * a)
            imaginaryRootImaginary = (math.sqrt(abs(discriminant))) / (2 * a)

            # Display imaginary roots
            print("\nThe imaginary roots are: ", format(imaginaryRootReal, ".2f") +
                  " + ", format(imaginaryRootImaginary, ".2f") + "i and",
                  format(imaginaryRootReal, ".2f"), "-",
                  format(imaginaryRootImaginary, ".2f") + "i")

         # Terminate iteration 
         else:
            print("\nBetter luck next time")
            
      # Calculate quadratic root
      elif discriminant == 0:
         root = (-b + math.sqrt(discriminant)) / (2 * a)

         # Display root
         print("\nThe root is:", format(root, ".2f"))

      # Calculate quadratic roots
      else:
         root1 = (-b + math.sqrt(discriminant)) / (2 * a)
         root2 = (-b - math.sqrt(discriminant)) / (2 * a)

         # Display roots
         if root1 < root2:
            print("\nThe roots are:", format(root1, ".2f"), "and",
                  format(root2, ".2f"))
         else:
            print("\nThe roots are:", format(root2, ".2f"), "and",
                  format(root1, ".2f"))

   #Prompt user if they wan to find a root
   findRoot = input("\nDo you wish to find a quadratic root? " +
                    "Enter 'Y' to continue: ")

print("\nGoodbye")


===== RESTART: C:\Users\wkrug\Desktop\assignment4_quadratic equation.py =====
Do you wish to find a quadratic root? Enter 'Y' to continue: Y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 1
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): -1
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): -6

The roots are: -2.00 and 3.00

Do you wish to find a quadratic root? Enter 'Y' to continue: y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 1.0
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): -15.0
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): 0.0

The roots are: 0.00 and 15.00

Do you wish to find a quadratic root? Enter 'Y' to continue: Y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 1.0
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): 0.0
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): -16.0

The roots are: -4.00 and 4.00

Do you wish to find a quadratic root? Enter 'Y' to continue: y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 3
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): 3
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): -6

The roots are: -2.00 and 1.00

Do you wish to find a quadratic root? Enter 'Y' to continue: Y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 1.0
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): 2.0
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): 1.0

The root is: -1.00

Do you wish to find a quadratic root? Enter 'Y' to continue: y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 0
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): 6
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): 3

You entered a linear equation...
Do you want the root of the linear equation? Enter 'Y' to continue: n

Better luck next time

Do you wish to find a quadratic root? Enter 'Y' to continue: Y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 0.0
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): 6.0
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): 3.0

You entered a linear equation...
Do you want the root of the linear equation? Enter 'Y' to continue: y

The linear root is: -0.50

Do you wish to find a quadratic root? Enter 'Y' to continue: Y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 0
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): 0
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): 21

You entered a linear equation...
Do you want the root of the linear equation? Enter 'Y' to continue: y

There is no root, this is a constant value: y = 21

Do you wish to find a quadratic root? Enter 'Y' to continue: Y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 1.0
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): -6.0
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): 13.0

Your equation has imaginary roots...
Do you want to find the imaginary roots? Enter 'Y' to continue: n

Better luck next time

Do you wish to find a quadratic root? Enter 'Y' to continue: y

Enter the 'a' coefficient of your quadratic equation (ax^2+bx+c): 1
Enter the 'b' coefficient of your quadratic equation (ax^2+bx+c): -6
Enter the 'c' coefficient of your quadratic equation (ax^2+bx+c): 13

Your equation has imaginary roots...
Do you want to find the imaginary roots? Enter 'Y' to continue: y

The imaginary roots are:  3.00 +  2.00i and 3.00 - 2.00i

Do you wish to find a quadratic root? Enter 'Y' to continue: n

Goodbye
>>> 
