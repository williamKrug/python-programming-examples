# Assignment #9
# William Krug
# CSCI 2001-90
# receive numeric grades from a list and print corresponding letter grade

def main():
   scores = getScores()
   printLetterGrades(scores)

# This function prompts the user to enter a numeric grade
def getNumericScore():
   score = eval(input("Enter the student's score. Type -999 when done: "))
   while not isValidScore(score):
      print("\n\t*** Invalid score ***")
      print("\t*** Only positive scores are allowed ***")
      score = eval(input("Enter the student's score. Type -999 when done: "))
   return score

# This function checks if the entered score is valid
def isValidScore(score):
   # Score is a positive number or sentinel value
   if score >= 0 or score == -999:
      isValid = True

   # Negative score entered
   else:
      isValid = False

   return isValid
   

# This function receives the numeric grades for input
def getScores():
   # Create an empty list of scores
   scores = []

   # Get the first student's score
   score = getNumericScore()

   while score != -999:
      # Add the student's score to the list
      scores.append(score)

      # Get the next student's score
      score = getNumericScore()
         
   return scores
   

# This function prints the letter grade for each numeric grade in the list
def printLetterGrades(listOfScores):
   print("\n\n*********************\n")
   for x in range(len(listOfScores)):
      # Maximum score possible
      best = 100

      # A grades
      if listOfScores[x] >= best - 10:
         print("Student", str(x) + "'s score of", listOfScores[x],
               "is an A")

      # B grades
      elif listOfScores[x] >= best - 20:
         print("Student", str(x) + "'s score of", listOfScores[x],
               "is a B")

      # C grades
      elif listOfScores[x] >= best - 30:
         print("Student", str(x) + "'s score of", listOfScores[x],
               "is a C")

      # D grades
      elif listOfScores[x] >= best - 40:
         print("Student", str(x) + "'s score of", listOfScores[x],
               "is a D")

      # F grades
      else:
         print("Student", str(x) + "'s score of", listOfScores[x],
               "is a F")

main()