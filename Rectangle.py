# Assignment #7
# William Krug
# CSCI 2001-90
# create and use a class with priviate fields and public methods

# define the Rectangle class
class Rectangle:
   # construct a rectangle object
   def __init__(self, width = 1, height = 2):
      self.__widthCheck = True
      self.__heightCheck = True
      self.__width = width
      self.__height = height

   def getWidth(self):
      return self.__width

   def getHeight(self):
      return self.__height

   def getWidthCheck(self):
      return self.__widthCheck

   def getHeightCheck(self):
      return self.__heightCheck

   def getArea(self):
      return self.__width * self.__height

   def getPerimeter(self):
      return (self.__width * 2) + (self.__height * 2)

   def setWidth(self, width):
      if width >= 0:
         self.__width = width
         self.__widthCheck = True
      else:
         print("\tThe width must be 0 or greater")
         self.__widthCheck = False

   def setHeight(self, height):
      if height >= 0:
         self.__height = height
         self.__heightCheck = True
      else:
         print("\tThe height must be 0 or greater")
         self.__heightCheck = False

def main():
   # prompt the user to continue the program
   answer = input("Do you want to find the AREA and PERIMETER\n" \
                  "of a rectangle?  Enter 'Y' to continue: ")

   # get WIDTH, HEIGHT, AREA and PERIMETER
   while answer.upper() == 'Y':

      print()
      
      # get the WIDTH and HEIGHT from the user
      width, height = getInputs()

      # Create a Rectangle object with user inputs
      rectangle = Rectangle()
      rectangle.setWidth(width)
      rectangle.setHeight(height)

      while not (rectangle.getWidthCheck() and rectangle.getHeightCheck()):
         print()
         width, height = getInputs()
         rectangle.setWidth(width)
         rectangle.setHeight(height)

      # display the area and perimeter
      printAreaAndPerimeter(rectangle.getWidth(), rectangle.getHeight(), \
                            rectangle.getArea(), rectangle.getPerimeter())

      # prompt the user to continue the program
      answer = input("\nDo you want to find the AREA and PERIMETER\n" \
                     "of a rectangle?  Enter 'Y' to continue: ")

   # exit program
   print("\n\tGoodbye")


# this function gets the WIDTH and HEIGHT from the user
def getInputs():
   width = eval(input("Enter the width of the rectangle: "))
   height = eval(input("Enter the height of the rectangle: "))
   return width, height

# this function prints the results
def printAreaAndPerimeter(width = 1, height = 1, area = 1, perimeter = 4):
   print("\nA", width, "by", height, "rectangle has an area of", \
         format(area, ".2f"), "and a perimeter of", format(perimeter, ".2f"))

main()