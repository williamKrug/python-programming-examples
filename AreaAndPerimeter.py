# Assignment #6
# William Krug
# CSCI 2001-90
# use methods to return the Area and Perimeter of rectangles

def main():
   # prompt the user to continue the program
   answer = input("Do you want to find the AREA and PERIMETER\n" \
                  "of a rectangle?  Enter 'Y' to continue: ")

   # get WIDTH, HEIGHT, AREA and PERIMETER
   while answer.upper() == 'Y':

      print()
      
      # get the WIDTH and HEIGHT from the user
      width, height = getInputs()

      # calculate the AREA
      area = getArea(width, height)

      # calculate the PERIMETER
      perimeter = getPerimeter(width, height)

      # display the results
      printAreaAndPerimeter(width, height, area, perimeter)

      # prompt the user to continue the program
      answer = input("\nDo you want to find the AREA and PERIMETER\n" \
                     "of a rectangle?  Enter 'Y' to continue: ")

   # exit program
   print("\n\tGoodbye")


# this function gets the WIDTH and HEIGHT from the user
def getInputs():
   width = eval(input("Enter the width of the rectangle: "))
   height = eval(input("Enter the height of the rectangle: "))
   return width, height

# this function finds the AREA of a rectangle
def getArea(width = 1, height = 1):
   return width * height

# this function finds the PERIMETER of a rectangle
def getPerimeter(width = 1, height = 1):
   return (width * 2) + (height * 2)

# this function prints the results
def printAreaAndPerimeter(width = 1, height = 1, area = 1, perimeter = 4):
   print("\nThe area of a", width, "by", height, "rectangle is", \
         format(area, ".2f"))
   print("The perimeter of a", width, "by", height, "rectangle is", \
         format(perimeter, ".2f"))

main()