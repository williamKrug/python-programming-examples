# Assignment #8
# William Krug
# CSCI 2001-90
# check if a password is valid using methods

def main():
   getPasscode = input("Enter 'Y' to continue: ")
   while getPasscode.upper() == 'Y':
      print()

      # Get the user's password
      password = getPassword()
      
      print()

      #Check the password and display if it is valid or not
      if isValid(password):
         print("\t" + password, "is a valid password.")
      else:
         print("\t" + password, "is an invalid password.")
         
      getPasscode = input("\nEnter 'Y' to continue: ")

   print("Goodbye")

def getPassword():
   # Explain the criteria for a password
   print("Enter your password below")
   print("Note: Your password must be an alphanumeric string")
   print("      consisting of at least 8 characters, two of which")
   print("      must be numbers.  Special characters are not allowed.")
   print()

   # Prompt the user for their password
   passcode = input("Password: ")

   return passcode

def isValid(passcode):
   valid = True

   # Check the length of the password
   if len(passcode) < 8:
      print("\tYour password must be at least 8 characters long.")
      valid = False

   # Check for at least 2 numbers
   numberCount = 0
   for i in range(len(passcode)):
      if passcode[i].isdigit():
         numberCount += 1

   if numberCount < 2:
      print("\tYour password must containt at least 2 numbers.")
      valid = False

   # Check for special characters
   if not passcode.isalnum():
      print("\tYour password can only contain numbers and letters.")
      print("\t  - No special characters are allowed.")
      valid = False

   return valid

main()